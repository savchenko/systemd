From: Aurelien Jarno <aurelien@aurel32.net>
Date: Wed, 19 Aug 2020 22:44:15 +0200
Subject: seccomp: add support for riscv64

This patch adds seccomp support to the riscv64 architecture. seccomp
support is available in the riscv64 kernel since version 5.5, and it
has just been added to the libseccomp library.

riscv64 uses generic syscalls like aarch64, so I used that architecture
as a reference to find which code has to be modified.

With this patch, the testsuite passes successfully, including the
test-seccomp test. The system boots and works fine with kernel 5.4 (i.e.
without seccomp support) and kernel 5.5 (i.e. with seccomp support). I
have also verified that the "SystemCallFilter=~socket" option prevents a
service to use the ping utility when running on kernel 5.5.

(cherry picked from commit f9252236c8618f7e0476667076b9a939f0e91967)
---
 src/nspawn/nspawn-oci.c   |  3 +++
 src/shared/seccomp-util.c | 30 ++++++++++++++++++++++++++----
 src/test/test-seccomp.c   |  3 +++
 3 files changed, 32 insertions(+), 4 deletions(-)

diff --git a/src/nspawn/nspawn-oci.c b/src/nspawn/nspawn-oci.c
index e3ade92..60a5909 100644
--- a/src/nspawn/nspawn-oci.c
+++ b/src/nspawn/nspawn-oci.c
@@ -1695,6 +1695,9 @@ static int oci_seccomp_arch_from_string(const char *name, uint32_t *ret) {
                 { "SCMP_ARCH_PPC",         SCMP_ARCH_PPC         },
                 { "SCMP_ARCH_PPC64",       SCMP_ARCH_PPC64       },
                 { "SCMP_ARCH_PPC64LE",     SCMP_ARCH_PPC64LE     },
+#ifdef SCMP_ARCH_RISCV64
+                { "SCMP_ARCH_RISCV64",     SCMP_ARCH_RISCV64     },
+#endif
                 { "SCMP_ARCH_S390",        SCMP_ARCH_S390        },
                 { "SCMP_ARCH_S390X",       SCMP_ARCH_S390X       },
                 { "SCMP_ARCH_X32",         SCMP_ARCH_X32         },
diff --git a/src/shared/seccomp-util.c b/src/shared/seccomp-util.c
index 9958592..9f14d3f 100644
--- a/src/shared/seccomp-util.c
+++ b/src/shared/seccomp-util.c
@@ -85,6 +85,8 @@ const uint32_t seccomp_local_archs[] = {
                 SCMP_ARCH_PPC64LE,     /* native */
 #elif defined(__powerpc__)
                 SCMP_ARCH_PPC,
+#elif defined(__riscv) && __riscv_xlen == 64 && defined(SCMP_ARCH_RISCV64)
+                SCMP_ARCH_RISCV64,
 #elif defined(__s390x__)
                 SCMP_ARCH_S390,
                 SCMP_ARCH_S390X,      /* native */
@@ -131,6 +133,10 @@ const char* seccomp_arch_to_string(uint32_t c) {
                 return "ppc64";
         case SCMP_ARCH_PPC64LE:
                 return "ppc64-le";
+#ifdef SCMP_ARCH_RISCV64
+        case SCMP_ARCH_RISCV64:
+                return "riscv64";
+#endif
         case SCMP_ARCH_S390:
                 return "s390";
         case SCMP_ARCH_S390X:
@@ -176,6 +182,10 @@ int seccomp_arch_from_string(const char *n, uint32_t *ret) {
                 *ret = SCMP_ARCH_PPC64;
         else if (streq(n, "ppc64-le"))
                 *ret = SCMP_ARCH_PPC64LE;
+#ifdef SCMP_ARCH_RISCV64
+        else if (streq(n, "riscv64"))
+                *ret = SCMP_ARCH_RISCV64;
+#endif
         else if (streq(n, "s390"))
                 *ret = SCMP_ARCH_S390;
         else if (streq(n, "s390x"))
@@ -1252,7 +1262,13 @@ int seccomp_protect_sysctl(void) {
 
                 log_debug("Operating on architecture: %s", seccomp_arch_to_string(arch));
 
-                if (IN_SET(arch, SCMP_ARCH_X32, SCMP_ARCH_AARCH64))
+                if (IN_SET(arch,
+                           SCMP_ARCH_AARCH64,
+#ifdef SCMP_ARCH_RISCV64
+                           SCMP_ARCH_RISCV64,
+#endif
+                           SCMP_ARCH_X32
+                          ))
                         /* No _sysctl syscall */
                         continue;
 
@@ -1336,6 +1352,9 @@ int seccomp_restrict_address_families(Set *address_families, bool allow_list) {
                 case SCMP_ARCH_MIPS64N32:
                 case SCMP_ARCH_MIPSEL64:
                 case SCMP_ARCH_MIPS64:
+#ifdef SCMP_ARCH_RISCV64
+                case SCMP_ARCH_RISCV64:
+#endif
                         /* These we know we support (i.e. are the ones that do not use socketcall()) */
                         supported = true;
                         break;
@@ -1573,7 +1592,7 @@ static int add_seccomp_syscall_filter(scmp_filter_ctx seccomp,
 }
 
 /* For known architectures, check that syscalls are indeed defined or not. */
-#if defined(__x86_64__) || defined(__arm__) || defined(__aarch64__)
+#if defined(__x86_64__) || defined(__arm__) || defined(__aarch64__) || (defined(__riscv) && __riscv_xlen == 64)
 assert_cc(SCMP_SYS(shmget) > 0);
 assert_cc(SCMP_SYS(shmat) > 0);
 assert_cc(SCMP_SYS(shmdt) > 0);
@@ -1618,13 +1637,16 @@ int seccomp_memory_deny_write_execute(void) {
                 case SCMP_ARCH_X86_64:
                 case SCMP_ARCH_X32:
                 case SCMP_ARCH_AARCH64:
-                        filter_syscall = SCMP_SYS(mmap); /* amd64, x32 and arm64 have only mmap */
+#ifdef SCMP_ARCH_RISCV64
+                case SCMP_ARCH_RISCV64:
+#endif
+                        filter_syscall = SCMP_SYS(mmap); /* amd64, x32, arm64 and riscv64 have only mmap */
                         shmat_syscall = SCMP_SYS(shmat);
                         break;
 
                 /* Please add more definitions here, if you port systemd to other architectures! */
 
-#if !defined(__i386__) && !defined(__x86_64__) && !defined(__powerpc__) && !defined(__powerpc64__) && !defined(__arm__) && !defined(__aarch64__) && !defined(__s390__) && !defined(__s390x__)
+#if !defined(__i386__) && !defined(__x86_64__) && !defined(__powerpc__) && !defined(__powerpc64__) && !defined(__arm__) && !defined(__aarch64__) && !defined(__s390__) && !defined(__s390x__) && !(defined(__riscv) && __riscv_xlen == 64)
 #warning "Consider adding the right mmap() syscall definitions here!"
 #endif
                 }
diff --git a/src/test/test-seccomp.c b/src/test/test-seccomp.c
index eec2779..051b5a2 100644
--- a/src/test/test-seccomp.c
+++ b/src/test/test-seccomp.c
@@ -73,6 +73,9 @@ static void test_architecture_table(void) {
                        "ppc\0"
                        "ppc64\0"
                        "ppc64-le\0"
+#ifdef SCMP_ARCH_RISCV64
+                       "riscv64\0"
+#endif
                        "s390\0"
                        "s390x\0") {
                 uint32_t c;
